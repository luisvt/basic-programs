// Dart Doesn't have interfaces since all classes are implicit interfaces
abstract class Person {
  String firtsName;
  String lastname;
  DateTime dob;
  
  factory Person() => new _Person();
  
  String sayHello();
  
}

class _Person implements Person{
  String firtsName;
  String lastname;
  DateTime dob;

  @override String sayHello() => 'hello';
}

class Employee extends _Person{
  String firtsName;
  String lastname;
  num salary;
  DateTime begginningDate;

  DateTime dob;
  
  @override String sayHello() => 'hi';
  
//  @override String sayHello() => 'hi' + super.sayHello();
}

main() {
  var employee1 = new Employee()
    ..firtsName = 'Luis'
    ..lastname = 'Vargas'
    ..salary = 100;
  
  var person1 = new Person()
    ..firtsName = 'Luis'
    ..lastname = 'Vargas';
  
  Person person2 = employee1;
  
  print(person2);
  
  List<Person> persons = [employee1, person1, person2];
  
  for(Person person in persons) {
    print(person.firtsName);
  }
  
  print( person1 == person2);
}