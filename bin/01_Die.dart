import 'dart:math';

class Die {
  static Random shaker = new Random();
  
  int sides, value;
  
  String toString() => '$value';
  
  Die({int n: 6}) {
    if( 4 <= n && n <= 20) { // if n is between 4 and 20
      sides = 6;
    } else {
      throw new ArgumentError("The value should be between 4 and 20");
    }
  }
  
  int roll() => value = shaker.nextInt(sides);
}
