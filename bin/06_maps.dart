main() {
  var gifts = {
    // Keys       Values
    'first': 'partridge',
    'second': 'turtledoves',
    'fifth': 'golden rings'
  };

  print('gifts:');
  for (var i = 0; i < gifts.length; i++) {
    print('${gifts.keys.elementAt(i)} : ${gifts.values.elementAt(i)}');
  }
  print('');

  var goldenRings = gifts['fifth'];

  //  print(goldenRings);

  var nobleGases = {
    // Keys  Values
    2: 'helium',
    10: 'neon',
    18: 'argon',
  };

  print('nobleGases');
  for (var nobleGas in nobleGases.keys) {
    print(nobleGas);
  }
  print('');

  var numberMap = {
    2: 5,
    3: 8,
    9: 3
  };

  numberMap.forEach((key, value) => print('$key : $value'));

  //  print(numberMap[9]);

}
