main() {
  var x = 1;
  var hex = 0xf;
  var bigInt = 346534658346524376592384765923749587398457294759347029438709349347999999999999;

  print("$x, $hex, $bigInt");
  
  var y = 1.1;
  var exponents = 1.42e5;
  
  print('$y, $exponents');
  
  // String -> int
  var one = int.parse('1');
  assert(one == 1);
  
  // String -> double
  var onePointOne = double.parse('1.1');
  assert(onePointOne == 1.1);
  
  // int -> String
  String oneAsString = 1.toString();
  assert(oneAsString == '1');
  
  // double -> String
  String piAsString = 3.14159.toStringAsFixed(2);
  assert(piAsString == '3.14');
  
  print(r'hello\tworld!');
  
  var s1 = 'String ' 'concatenation'
      " works even over line breaks.";
  print(s1);
}