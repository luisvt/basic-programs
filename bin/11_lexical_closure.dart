/// Returns a function that adds [addBy] to a number.
Function makeAdder(num addBy) {
  adder(num i) {
    return addBy + i;
  }
  return adder;
}

main() {
  Function add2 = makeAdder(2); // Create a function that adds 2.
  var add4 = makeAdder(4); // Create a function that adds 4.

  print(add2);
  print(add2(3));
  
  print(add4);
  print(add4(5));
}