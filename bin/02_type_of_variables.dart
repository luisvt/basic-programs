void main() {
  //primitive
  const double number = 42.1; //numbers, string, list, maps, object.
                    // num -> int, int8, int16, int32, int64, long, longlong
                    // num -> double float, 
  // printf("The number is %f", number)
  
//  number = 33;
  print('The number is $number'); //ctrl+r
  
  String gretting = "hello";
  
//  print(gretting + 3); //warning
  
  print("$gretting world!");
  
//  List<double> intList = [1, 2, 3, 4,"hello"]; //no warning
//  var intList = <int>[1, 2, 3, 4, 'hello']; //warning
  var intList = <int>[1, 2, 3, 4];
  
  print(intList[0] + 2);
//  print(intList[4] + "hi"); //no warning but throws error
  
  print("$intList");
  
//  List<String> stringList = [2, 3, "hello"];
  var stringList = ['hi', 'hello'];

//  print(intList[0] + 2); //no warning but throws error
  print(stringList[1] + '!');
  
  print(stringList);
  
  Map<String, int> someMap = {
     "hello": 1,
     "hi": "bonjour"
  };
  
  print(someMap);
}
