import "dart:io";

void main() {
  String decission;
  do{
    print('Type your Grade [0-10]');
    int grade = int.parse(stdin.readLineSync());
    
    if(grade < 0 || grade > 10) {
      print('You should enter a value between 0 and 10');
      print('Desea Continuar [Y]:');
      decission = stdin.readLineSync();
      if(decission == 'Y' || decission == 'y') continue;
      else break;
    }
    
    switch (grade) {
      case 10: //if(Grade == 'A')
        print("Excellent");
        break;
      case 9: // else if(...
        print("Good");
        break;
      case 8:
        print("OK");
        break;
      case 7:
        print("Mmmmm....");
        break;
      case 6:
        print("You must do better than this");
        break;
      default:
        print("What is your grade anyway?");
    }
    print('Desea Continuar [Y]:');
    decission = stdin.readLineSync();
  }while(decission == 'Y' || decission == 'y');
}
